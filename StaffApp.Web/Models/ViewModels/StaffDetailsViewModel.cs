using System;

namespace StaffApp.Web.Models.ViewModels
{
    public class StaffDetailsViewModel
    {
        public string StaffIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficeName { get; set; }
    }
}