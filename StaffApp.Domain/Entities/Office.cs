using System;

namespace StaffApp.Domain.Entities
{
    public class Office 
    {
        public int OfficeId { get; set; }
        public string OfficeName { get; set; }
    }
}    