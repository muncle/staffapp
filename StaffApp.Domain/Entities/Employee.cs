﻿using System;

namespace StaffApp.Domain.Entities
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string StaffIdNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
