﻿using System;
using Microsoft.EntityFrameworkCore;
using StaffApp.Domain.Entities;

namespace StaffApp.Persistence
{
    public class StaffAppDbContext : DbContext
    {
        public StaffAppDbContext(DbContextOptions<StaffAppDbContext> options)
            : base(options)
        {

        }
            public DbSet<Employee> Employees { get; set; }
            public DbSet<Office> Offices { get; set; }
    
    }
}
